// Global variables

//var urlWebService = 'http://172.16.2.113:8001/SCADA/'
var urlWebService = 'http://172.16.2.71:8001/SCADA/'

var arrayMenuLevel = [];
var arrayMenu = ['overview', 'recipes', 'maintenance', 'settings', 'subsystems', 'history log'];
var arrayMenuFiles = ['overview.html', 'recipes.html', 'maintenance.html', 'settings.html', 'subsystems.html', 'history.html'];

var key = "abc123xyz";


// Global functions

function showMessage(message = "")      
{
    $( "#dialog" ).html(message);
    $( "#dialog" ).dialog({
        autoOpen: true,
        modal: true,
        dialogClass: 'hide-close',
        closeOnEscape: false,
        buttons: {
            "Ok": function () {
                $(this).dialog("close"); 
            }
        } 
    });
}


function validateToken()
{
    if(localStorage.length > 0){
        var userName = localStorage.getItem(btoa("username"));
        var password = localStorage.getItem(btoa("password"));
        var level = localStorage.getItem(btoa("level"));
        
        var objLoginUser = {
            username: CryptoJS.AES.decrypt(userName, key).toString(CryptoJS.enc.Utf8),
            password: CryptoJS.AES.decrypt(password, key).toString(CryptoJS.enc.Utf8)
        };
        
        var objJson = {
            'Login': JSON.stringify(objLoginUser)
        };

        $.ajax({
            url: urlWebService,
            type: 'POST',
            dataType: 'json',
            data: objJson,
            success: function(data) {
                if(data.CorrectLog){
                    localStorage.setItem("sw", true);
                 }else{
                    localStorage.setItem("sw", false);
                    window.location = "login.html"; 
                }
            }
        }).done(function(){
           
        });
    }else{
        localStorage.setItem("sw", false);
    }
}


// Function to generate menus dinamically
function generateMenu()
{
    var content = "";
    var posElement;
    arrayMenuLevel.forEach(function(element, index){
        posElement = findElementArray(arrayMenu, element);
        content += "<h5 class='text-menu'>";
        content += "<a href='"+ arrayMenuFiles[posElement] + "' class='nav-item nav-link padding20'>" + element + "</a>";
        content += "</h5>";
    });
    content += "</div>";
    $("#nav-menu").html(content);
}


// Function to paint the datetime in application
function timeRequest()
{
    var objJson ={
        'Update_Time':JSON.stringify(true)
    };
    $.ajax({
        url: urlWebService,
        type: 'POST',
        dataType: 'json',
        data: objJson,
        success: function(data) {
            $("#element-time").text(data.DATE + " - " + data.TIME);
        }
    }).done(function(){

    });
}


// Function to find an element in an array
function findElementArray(arrayData, datum)
{
    var pos = -1;
    var i = 0;
    while(i < arrayData.length && pos === -1){
        if(arrayData[i] === datum){
            pos = i;
        }else{
            i++;
        }
    }
    return pos;
}


//https://www.lawebdelprogramador.com/foros/JavaScript/1127366-javascript-solo-numeros-para-siempre.html
// Function to validate only integer numbers in the input numeric data
function validateOnlyNumbers(e)
{
    var key = window.Event ? e.which : e.keyCode
    return (key >= 48 && key <= 57)
}


// https://gist.github.com/Javlopez/944773
// Function to validate only float numbers in the input numeric data
function filterFloat(evt, input)
{
    // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
    var key = window.Event ? evt.which : evt.keyCode;    
    var chark = String.fromCharCode(key);
    var tempValue = input.value + chark;
    if(key >= 48 && key <= 57){
        if(!filter(tempValue)){
            return false;
        }else{       
            return true;
        }
    }else{
        if(key === 8 || key === 13 || key === 0) {     
            return true;              
        }else if(key === 46){
            if(!filter(tempValue)){
                return false;
            }else{       
                return true;
            }
        }else{
            return false;
        }
    }
}


function filter(__val__)
{
    var preg = /^([0-9]+\.?[0-9]{0,2})$/; 
    if(preg.test(__val__)){
        return true;
    }else{
       return false;
    }
}


function fullScreen(element)
{
    //Si el navegador es Mozilla Firefox
    if(element.mozRequestFullScreen){
        element.mozRequestFullScreen();
    }
    //Si el navegador es Google Chrome
    else if(element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen();
    }
    //Si el navegador es otro
    else if(element.requestFullScreen) { 
        element.requestFullscreen(); 
    }
}


// Configurations for the dropdown menu

var timeout    = 500;
var closetimer = 0;
var ddmenuitem = 0;

function jsddm_open()
{  
    jsddm_canceltimer();
    jsddm_close();
    ddmenuitem = $(this).find('ul').css('visibility', 'visible');
}

function jsddm_close()
{  
    if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');
}

function jsddm_timer()
{  
    closetimer = window.setTimeout(jsddm_close, timeout);
}

function jsddm_canceltimer()
{  
    if(closetimer){  
        window.clearTimeout(closetimer);
        closetimer = null;
    }
}

document.onclick = jsddm_close;
// End confgurations for the dropdown menu


