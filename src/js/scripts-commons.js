/* 
 * Scripts commons for the application
 */

$(document).ready(function(){  
    $('#jsddm > li').bind('mouseover', jsddm_open);
    $('#jsddm > li').bind('mouseout',  jsddm_timer);
});


$('body').ready(function(){
    if(localStorage.getItem(btoa("username")) !== null){
        var userEncrypt = localStorage.getItem(btoa("username"));
        var user = CryptoJS.AES.decrypt(userEncrypt, key).toString(CryptoJS.enc.Utf8);
//        $("#jsddm").find("li").find("ul").find("li").find("#link-login").text(user)
        $("#link-login").text(user);
        switch($('body').find('input').attr('data-page')){
            case 'records':
                validateToken();
                if(localStorage.getItem("sw") === 'false'){
                    $( "#dialog" ).html("Invalid credentials. You aren't authorized to access this page");
                    $( "#dialog" ).dialog({
                        autoOpen: true,
                        modal: true,
                        dialogClass: 'hide-close',
                        closeOnEscape: false,
                        buttons: {
                            "Ok": function () {
                                $(this).dialog("close");
                                window.location = "login.html"; 
                            }
                        } 
                    });
                } 
                break;
        }
    }else{
        $( "#dialog" ).html("Invalid credentials. You aren't authorized to access this page");
        $( "#dialog" ).dialog({
            autoOpen: true,
            modal: true,
            dialogClass: 'hide-close',
            closeOnEscape: false,
            buttons: {
                "Ok": function () {
                    $(this).dialog("close");
                    window.location = "login.html"; 
                }
            } 
        });
    }
});


//$("body").find("#navbar-menu").find("ul").find("li").find("#link-logout").click(function(){
$('#navbar-menu').delegate('#link-logout', 'click', function(){
    $( "#dialog" ).html("¿Are you sure to log out?");
    $( "#dialog" ).dialog({
        autoOpen: true,
        modal: true,
        dialogClass: 'hide-close',
        closeOnEscape: false,
        buttons: {
            "Ok": function(){
                $(this).dialog("close"); 
                localStorage.clear();
                window.location = 'login.html';
            },
            "Cancel": function(){
                $(this).dialog("close");                            
            }
        } 
    });
});


$("#link-users").click(function(){
    var levelEncrypt = localStorage.getItem(btoa("level"));
    var level = CryptoJS.AES.decrypt(levelEncrypt, key).toString(CryptoJS.enc.Utf8);
    if(level !== "admin"){
        $( "#dialog" ).html("You don't have permission for access this page");
        $( "#dialog" ).dialog({
            autoOpen: true,
            modal: true,
            dialogClass: 'hide-close',
            closeOnEscape: false,
            buttons: {
                "Ok": function(){
                    $(this).dialog("close"); 
                }
            } 
        });
    }else{
        window.location = 'users.html';
    }
});


$("#link-profile").click(function(){
    window.location = 'users-change-password.html';
});